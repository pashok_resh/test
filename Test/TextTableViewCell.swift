//
//  TextTableViewCell.swift
//  Test
//
//  Created by Павел Мартыненков on 13.03.17.
//  Copyright © 2017 Павел Мартыненков. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
