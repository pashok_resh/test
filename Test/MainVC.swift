//
//  ViewController.swift
//  Test
//
//  Created by Павел Мартыненков on 13.03.17.
//  Copyright © 2017 Павел Мартыненков. All rights reserved.
//

import UIKit

class MainVC: UITableViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    let pickerView = UIPickerView()
    
    let queue = OperationQueue()
    
    var tableData:Array<Dictionary<String, Any>>! = Array<Dictionary<String, Any>>()
    var formTitle:String!
    var formDictionary: Dictionary<String, String> = Dictionary()
    var editingTextFieldTag = 0
    var sortedKeys:Array<String>!
    var textFieldValues: [String] = Array()
    var postAnswer: String!
    

    override init(style: UITableViewStyle){
        super.init(style: style)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        self.tableView.register(UINib(nibName: "TextTableViewCell", bundle:nil), forCellReuseIdentifier: "Text")
        self.tableView.register(UINib(nibName: "ButtonTableViewCell", bundle:nil), forCellReuseIdentifier: "Button")

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.downloadForms()
    }
    

    
    // MARK: - Tableview Data Source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableData.count != 0 ? 2 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.tableData.count : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            var tempDictionary = self.tableData[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Text", for: indexPath as IndexPath) as! TextTableViewCell
            
            cell.titleLabel.text = tempDictionary["title"] as! String?
            
            switch tempDictionary["type"] as! String {
            case "TEXT":
                cell.valueTextField.tag = (1 * 100 + indexPath.row)
                
            case "NUMERIC":
                cell.valueTextField.tag = (2 * 100 + indexPath.row)
                
            case "LIST":
                cell.valueTextField.tag = (3 * 100 + indexPath.row)
                cell.valueTextField.inputView = self.pickerView
            
            default:
                cell.valueTextField.tag = 0
            }
            
            cell.valueTextField.delegate = self
            cell.valueTextField.text = self.textFieldValues[indexPath.row]
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Button", for: indexPath as IndexPath) as! ButtonTableViewCell
            
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            
//            self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.contentView.endEditing(true)
//            self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.contentView.endEditing(true)
            
            let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! TextTableViewCell
            cell.valueTextField.becomeFirstResponder()
            cell.valueTextField.resignFirstResponder()
            self.queue.addOperation {
                self.sendDataForms()
            }
        }
        
         tableView.cellForRow(at: indexPath)?.isSelected = false
        
        
    }
    
    // MARK: - Download forms
    
    func downloadForms() {
        
        let alertController = UIAlertController(title: "Загрузка форм\n\n", message: nil, preferredStyle: .alert)
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.center = CGPoint(x: 135, y: 80)
        spinner.color = UIColor.black
        spinner.startAnimating()
        alertController.view.addSubview(spinner)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { action in
            self.queue.cancelAllOperations()
            print("Загрузка отменена")
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
    
        }
        
        self.queue.addOperation {
            self.getDataFromUrl("http://test.clevertec.ru/tt/meta")
        }
    }

    
    func getDataFromUrl(_ link:String) {
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            self.extractJSON(data!)
        })
        task.resume()
    }
    
    func extractJSON(_ data: Data) {
        let json: Any?
        
        do {
            json = try JSONSerialization.jsonObject(with: data, options: [])
        } catch {
            return
        }
        
        guard let dataList = json as? Dictionary<String, Any> else
        {
            return
        }
        
        print(dataList)
        
        if let titleForm = dataList["title"] as? String {
            self.formTitle = titleForm
        }
        
        if let dataArray = dataList["fields"] as? [Dictionary<String, Any>] {
            
            for i in 0...(dataArray.count - 1){
                
                var dictionary = dataArray[i]
                var dictionaryToAdd = [String: Any]()
              
                if let title = dictionary["title"] as? String {
                   dictionaryToAdd["title"] = title
                }
                
                if let name = dictionary["name"] as? String {
                    dictionaryToAdd["name"] = name
                }
                
                if let type = dictionary["type"]  as? String {
                    dictionaryToAdd["type"] = type
                    
                    if type == "LIST" {
                        if let valuesList = dictionary["values"] as? Dictionary<String, String> {
                            self.formDictionary = valuesList
                            self.sortedKeys = Array<String>()
                            self.sortedKeys.append(contentsOf: self.formDictionary.keys.sorted())
                        }
                    }
                }
                    self.tableData.append(dictionaryToAdd)
            }
            
            self.textFieldValues = [String](repeating: "", count: self.tableData.count)
            
        }
        
        self.queue.addOperation {
            self.hideAlert()
        }
        
    }
    func hideAlert() {
            self.dismiss(animated: true, completion: {
                        self.title = self.formTitle
                        self.pickerView.reloadAllComponents()
                        self.tableView.reloadData()
                })
        }
    
    //MARK: Upload Data
    
    func sendDataForms() {
        
        let alertController = UIAlertController(title: "Отправка данных\n\n", message: nil, preferredStyle: .alert)
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.center = CGPoint(x: 135, y: 80)
        spinner.color = UIColor.black
        spinner.startAnimating()
        alertController.view.addSubview(spinner)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { action in
            self.queue.cancelAllOperations()
            print("Отправка отменена")
        }
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
        }
    
        self.queue.addOperation {
            self.uploadData()
        }
        
    }
    
    func showAnswer() {
        
        let alertController = UIAlertController(title: "Данные отправлены", message: self.postAnswer, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in }
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true) {}
    }
    
    func uploadData() {
        
        let url = URL(string: "http://test.clevertec.ru/tt/data/")
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: self.creatingJSON(), options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                    
                    if let answer = json["result"] as? String {
                        self.postAnswer = answer
                    }
                    print(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
        self.queue.addOperation {
            self.dismiss(animated: true) {
                self.showAnswer()
            }
        }
        
    }
    
    
    func creatingJSON() -> Dictionary<String, Any> {
        
        var jsonDictionary = Dictionary<String, Dictionary<String,String>>()
        var formDictionary = Dictionary<String,String>()
        
        for i in 0...(self.tableData.count - 1) {
            
            let cell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! TextTableViewCell
            
            let tag = cell.valueTextField.tag
            
            let typeTag = (tag - (tag % 100)) / 100
            
            let name = self.tableData[i]["name"] as! String
            
            switch typeTag {
            case 1:
                formDictionary[name] = self.textFieldValues[i]
                
            case 2:
                let num = self.textFieldValues[i]

                    if (Float(num) != nil) && (num != "") {
                       formDictionary[name] = "\(Float(num)!)"
                    } else {
                        formDictionary[name] = ""
                    }
                
                
            case 3:
                formDictionary[name] = self.valueForIndexInList(index: i)
                
            default: break
            }
            
        }
        
        jsonDictionary["form"] = formDictionary
        
        print(jsonDictionary)
        
        return jsonDictionary
    }
    
    func valueForIndexInList(index: Int) -> String {
        
        switch self.textFieldValues[index] {
        case "---":
            return "empty"
            
        case "Первое значение":
            return "k1"
            
        case "Второе значение":
            return "k2"
            
        case "Третье значение":
            return "k3"
            
        case "Четвёртое значение":
            return "k4"
            
        default:
            return"empty"
        }
    
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        textField.resignFirstResponder()
    
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.editingTextFieldTag = textField.tag
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let row = textField.tag % 100
        
        self.textFieldValues[row] = textField.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let mainTag = (textField.tag - (textField.tag % 100)) / 100
        
        if mainTag == 2 {
            
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            
            if filtered == string {
                
                return true
            } else {
                
                if string == "." {
                    
                    let countdots = textField.text!.components(separatedBy:".").count - 1
                    if countdots == 0 {
                        
                        return true
                    }else{
                        
                        if countdots > 0 && string == "." {
                            
                            return false
                        } else {
                            
                            return true
                        }
                    }
                }else{
                    
                    return false
                }
            }
        } else {
            
            return true
        }
    }
    
    //MARK: Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.sortedKeys.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.formDictionary[self.sortedKeys[row]]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let indexPath = IndexPath(row: editingTextFieldTag % 100, section: 0)
        
        let cell = self.tableView.cellForRow(at: indexPath) as! TextTableViewCell
        
        cell.valueTextField.text = self.formDictionary[self.sortedKeys[row]]
        cell.valueTextField.resignFirstResponder()
    }

}


